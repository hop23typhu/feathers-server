

module.exports = {
  before: {
    all: [
      // Use normal functions
      // function (hook) { console.log('before all hook ran'); }
    ],
    find: [
      // function (hook) {
      //   console.log(hook.result, 'hook.result');
      //   let { params: { query } } = hook;
      //   const $search = hook.params.query.search;
      //   return hook;
      // },
      function (hook) {
        // let query = {
        //   email: {
        //     $in: ['luonghop.lc@gmail.comxxxx', 'Bob']
        //   }
        // };
        // hook.params.query = query;

        //read only
        console.log(hook.type, 'type');
        console.log(hook.path, 'path');
        console.log(hook.method, 'method ');

        //writeable
        console.log(hook.id, 'id');
        console.log(hook.result, 'result');
        console.log(hook.data, 'data  ');
        console.log(hook.error, 'error  ');


        // return hook.app.service('contact').find().then(contact => {
        //   console.log(contact,'contact');
        //   return hook;
        // });

      }
    ],
    get: [],
    create: [
      function (hook) {
        hook.data.gender = 'Male';
        return hook;
      }
    ],
    update: [
      function (hook) {
        hooks.disallow('external')
      }
    ],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [
      // function (hook) {
      //   // let query = {
      //   //   email: {
      //   //     $in: ['luonghop.lc@gmail.comxxxx', 'Bob']
      //   //   }
      //   // };
      //   // hook.params.query = query;

      //   //read only
      //   console.log(hook.type, 'type');
      //   console.log(hook.path, 'path');
      //   console.log(hook.method, 'method ');

      //   //writeable
      //   console.log(hook.id, 'id');
      //   console.log(hook.result, 'result');
      //   console.log(hook.data, 'data  ');
      //   console.log(hook.error, 'error  ');


      //   // hook.service.find({$limit:1});
      //   return hook;
      // }
      function (hook) {
        // let query = {
        //   email: {
        //     $in: ['luonghop.lc@gmail.comxxxx', 'Bob']
        //   }
        // };
        // hook.params.query = query;

        //read only
        console.log(hook.type, 'type');
        console.log(hook.path, 'path');
        console.log(hook.method, 'method ');

        //writeable
        console.log(hook.id, 'id');
        console.log(hook.result, 'result');
        console.log(hook.data, 'data  ');
        console.log(hook.error, 'error  ');
        const userId = hook.result.userId;

        // return hook.app.service('contact').get(userId).then(contact => {
        //   hook.result.additional = { hello: true };
        //   console.log(contact, 'contact');
        //   return hook;
        // });

      }
    ],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
