const contact = require('./contact/contact.service.js');
const users = require('./users/users.service.js');
const todo = require('./todo/todo.service.js');
module.exports = function () {
  const app = this; // eslint-disable-line no-unused-vars
  app.configure(contact);
  app.configure(users);
  app.configure(todo);
};
