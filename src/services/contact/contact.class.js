'use strict';

class Service {
  constructor(options) {
    this.options = options || {};
  }
  find(params) {
    console.log(params,'params');
    return Promise.resolve([
      {
        id: 1,
        text: 'Message 1xxxxxxx'
      }, {
        id: 2,
        text: 'Message 2xxxx'
      }
    ]);
  }
  get(id, params) {
    return Promise.resolve({hop:'123'})
  }
  create(data, params) { }
  update(id, data, params) { }
  patch(id, data, params) { }
  remove(id, params) { }
  setup(app, path) { }
}

module.exports = function (options) {
  return new Service(options);
};

module.exports.Service = Service;
